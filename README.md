# Who Owns It?

[![Get the addon](get-the-addon-small.png)](https://addons.mozilla.org/en-US/firefox/addon/who-owns-it/)

Identifies the company who ultimately owns each site you visit.

This small WebExtension adds the icon of the parent company who owns each site
you visit to the address bar.

![Screenshot showing the icon added to the browser UI](screenshot.png)

This extension tracks no data, and makes no network requests.

## How to help?

You can improve the extension by adding companies and their domain names to
[companies.js](companies.js).

## Installation for development

1. Clone this repository
2. Go to about:debugging in Firefox
3. Click the "Load Temporary Add-on..." button
4. Select any file in this repository
5. Visit [https://github.com](github.com) and notice the Microsoft logo on the right side of the address bar
